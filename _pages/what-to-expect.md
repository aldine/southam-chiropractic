---
title: What to Expect
layout: default
permalink: /what-to-expect/
banner:
    image: 713185930
    text: Getting you stronger
three_images:
    - 244733887
    - 636736618
    - 1335251387
---

# {{page.title}}
## Initial Consultation
Different people come to us for different reasons (symptom relief, improving function, preventative care, maintaining health or simply for a check-up). So, the first thing we do is to find out what we can help you with. Our Initial Consultation includes a case history, an examination (involving assessment of your spine, joints, nerves and muscles using orthopaedic, functional, and neurological tests). It takes 30-40 minutes for this consultation.

{% include three_images %}

## Report of Findings
After the initial consultation, your chiropractor will analyse the results of your history and test resuts and put together a report of findings. They will then discuss this with you and explain the best course of action and treatment plan (if required) to suit your individual needs.
Under normal circumstances the report of findings will happen on a different day to your initial consultation, so that you have time to process the information given to you and consider any questions you might have.

## Fees
Our treatments start from as little as &pound;30.